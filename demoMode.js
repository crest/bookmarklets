(function() {
  /**
   * The data map used for mocking.  Keys are the uuids for the individual to
   * be mocked.  The firstName and lastName attributes are then used for updating
   * the person's firstName, lastName, firstInitial, name, and qualifiedDisplayName.
   */
  var MOCK_DATA_MAP = {
    1287881 : { realName: "Lauren Paige Magruder", firstName: 'Helena', lastName: 'Sanchez' },
    1291887 : { realName: "Jane Lee", firstName: 'Eliza', lastName: 'Moore' },
    1702274 : { realName: "Na Ren", firstName: 'Lacey', lastName: 'Roberts' },
    1109737 : { realName: "Amy Morris Burchett", firstName: 'Patricia', lastName: 'Hobbs-Keys' },
    970134  : { realName: "Amy M Burchett", firstName: 'Patricia', lastName: 'Hobbs-Keys' },
    1375710 : { realName: "Jon David Couch Sr", firstName: 'Richard', lastName: 'Patil' },
    811400  : { realName: "Joseph A Schetz", firstName: 'Shiv', lastName: 'Abedi' },
    1199514 : { realName: "Craig Woolsey", firstName: 'Christopher', lastName: 'Sehgal' },
    1185465 : { realName: "Aurelien Borgoltz", firstName: 'Thomas', lastName: 'Carter' },
    1157331 : { realName: "Todd Lowe", firstName: 'Dawn', lastName: 'Mai Lui' },
    1656840 : { realName: "K Todd Lowe", firstName: 'Dawn', lastName: 'Mai Lui' },
    815950  : { realName: "Rakesh Kapania", firstName: 'Jordan', lastName: 'Wegena' },
    1218427 : { realName: "Danesh Tafti", firstName: 'Salvador', lastName: 'Osborne' },
    1376480 : { realName: "Srinath Ekkad", firstName: 'Herbert', lastName: 'Lynch' },
    1892740 : { realName: "Alan Thomas Asbeck", firstName: 'Joe', lastName: 'Cook' },
    1142768 : { realName: "Beth Howell", firstName: 'Meredith', lastName: 'Wilkerson' },
    1287271 : { realName: "Melissa J Williams", firstName: 'Monique', lastName: 'Morales' },
    1700749 : { realName: "Eric G Paterson", firstName: "Vanjan", lastName: "Persinger" },
    1218427 : { realName: "Danesh K Tafti", firstName: "Johnathan", lastName: "Pennington" },
    1240450 : { realName: "Ross M Verbrugge", firstName: "Meredith", lastName: "Wilkerson" },
    809532  : { realName: "John J Lesko", firstName: "Lloyd", lastName: "Rocha" },
    1142067 : { realName: "Bradley P Martens", firstName: "Ricardo", lastName: "Camacho" },
    1145842 : { realName: "Edward L Nelson", firstName: "Georgo", lastName: "Uzzi" },
    1147925 : { realName: "Dawn Lee Bishop", firstName: "Christa", lastName: "Combs" },
    808909  : { realName: "Faythe A Rittenhouse", firstName: "Kassandra", lastName: "Davidson" }
  };
  //////////////////////// SHOULDN'T MODIFY BELOW HERE ////////////////////////
  var $state = angular.element(document.body).injector().get('$state');
  if (window.demoModeEnabled !== undefined) {
    if (window.demoModeEnabled) {
      window.demoModeEnabled = false;
      alert("Demo mode has been disabled. Reloading view now.");
    }
    else {
      window.demoModeEnabled = true;
      alert("Demo mode has been re-enabled. Reloading view now.");
    }
    $state.go($state.current, $state.params, { reload : true, inherit: false, notify: true });
    return;
  }
  if (viewingProposal()) {
    alert("Unable to start demo mode while a proposal is opened. Return to a blank editor and try again");
    return;
  }
  
  // Override the $http service's get, put, post, and delete methods
  var $http = angular.element(document.body).injector().get('$http');
  $http.get = overrideHttpMethod($http.get, 1);
  $http.put = overrideHttpMethod($http.put, 2);
  $http.post = overrideHttpMethod($http.post, 2);
  $http.delete = overrideHttpMethod($http.delete, 1);
  // Add a WebSocket listener that will modify the data before published on the
  // EventBus
  angular.element(document.body).injector()
      .get('WebSocketService')
      .registerListener('.*', websocketListener);
  updateData(angular.element(document.body).injector()
      .get('UserService')
      .getCurrentUser());
  window.demoModeEnabled = true;
  alert("Demo mode has been enabled. Reloading current view");
  $state.go($state.current, $state.params, { reload : true, inherit: false, notify: true });
  /**
   * Create an overrided/wrapped method for one of the $http service methods
   * @param originalMethod {fn} The original function/method that is being overridden
   * @param configArg {int} The argument index on the originalMethod in which 
   *                        the config is located
   * @return A new function that effectively wraps the original $http service method                          
   */
  function overrideHttpMethod(originalMethod, configArg) {
    return function() {
      var config = (arguments.length == configArg + 1) ? arguments[configArg] : null;
      var promise = originalMethod.apply(window, arguments)
          .then(function(response) {
            if (window.demoModeEnabled)
              updateData(response.data);
            return response;
          });
      // Add the success/error handlers, as some of the code is still using them
      promise.success = function(fn) {
        promise.then(function(response) {
          fn(response.data, response.status, response.headers, config);
        });
        return promise;
      };
      promise.error = function(fn) {
        promise.then(null, function(response) {
          fn(response.data, response.status, response.headers, config);
        });
        return promise;
      };
      return promise;
    };
  }
  /**
   * Recursive function that finds all people-like objects and updates them to
   * the mocked values, if a mocked value is to be found.
   * @param data {*} The data type to analyze.
   */
  function updateData(data) {
    if (typeof data != 'object')
      return;
    for (var key in data) {
      updateData(data[key]);
    }
    if (data.guid !== undefined && MOCK_DATA_MAP[data.guid] !== undefined) {
      var person = MOCK_DATA_MAP[data.guid];
      console.log("-- Mocking person with guid: " + data.guid + " and name: " + person.realName);
      setField(data, 'firstInitial', person.firstName.charAt(0));
      setField(data, 'firstName', person.firstName);
      setField(data, 'lastName', person.lastName);
      setField(data, 'name', person.firstName + ' ' + person.lastName);
      setField(data, 'fullName', person.firstName + ' ' + person.lastName);
      setField(data, 'qualifiedDisplayName', data.name + ' (' + data.organizationCode + ')');
      setField(data, 'displayName', person.firstName + ' ' + person.lastName);
    }
    // Check notification
    else if (data.sender !== undefined && data.text !== undefined) {
      for (var key in MOCK_DATA_MAP) {
        var name = MOCK_DATA_MAP[key];
        if (data.text.indexOf(name.realName) > -1) {
          console.log("-- Mocking notification with name: " + name.realName);
          data.text = data.text.replace(new RegExp(name.realName, 'g'), name.firstName + " " + name.lastName)
        }
      }
    }
    // Check comment authors
    else if (data.author !== undefined && data.author.name !== undefined) {
      for (var key in MOCK_DATA_MAP) {
        var name = MOCK_DATA_MAP[key];
        if (data.author.name.indexOf(name.realName) > -1) {
          console.log("-- Mocking notification with name: " + name.realName);
          data.author.name = data.author.name.replace(new RegExp(name.realName, 'g'), name.firstName + " " + name.lastName)
        }
      }
    }
  }
  /**
   * The WebSocketService listener that modifies the incoming message data
   * @param data {object} The WebSocket data
   */
  function websocketListener(data) {
    if (data.proposal === undefined || !window.demoModeEnabled)
      return;
    updateData(data.proposal);
  }
  function viewingProposal() {
    return $state.params && $state.params.proposalId;
  }
  function setField(obj, fieldName, data) {
    if (obj[fieldName] !== undefined)
      obj[fieldName] = data;
  }
})();