(function() {
if (!(jQuery().highcharts)) {
  var s=document.createElement('script');
  s.src='//code.highcharts.com/highcharts.js';
  document.getElementsByTagName('body')[0].appendChild(s);
}
var check = function(callback) {
  if (jQuery().highcharts) return callback();
  setTimeout(function() { check(callback); }, 100);
};
var go = function() {
  if (typeof proposals === 'undefined')
    return alert("Hmm... don't see any proposals to work with");
  if ($("#graph-container").size() > 0)
    return alert("Please close the current graph before trying to load it again");
  var assignmentData = {};
  proposals.forEach(function(summary) {
    if (!(summary.assignee.name in assignmentData))
      assignmentData[ summary.assignee.name ] = 0;
    assignmentData[ summary.assignee.name ]++;
  });
  var numProposals = proposals.length;
  var dataset = [];
  for (var key in assignmentData) {
    if (typeof assignmentData[key] === 'function') continue;
    var value = assignmentData[key];
    dataset.push({ name: key, y: parseFloat((value / numProposals * 100).toFixed(1)), count : value })
  }
  var $modal = $("<div><div id='graph-modal' class='modal fade in' style='display:block;'><div class='modal-dialog'><div class='modal-content'><div class='modal-header'><h4 class='modal-title'>Assignee Data</h4></div><div class='modal-body'><div id='graph-container' style='min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto'></div><a id='close-graph-display'>Close</a></div></div></div></div></div>");
  $("body").append($modal);
  $("#close-graph-display").click(function() { $("#graph-modal").remove(); });
  displayChart($("#graph-container"), dataset);
};
var displayChart = function($container, data) {
  $container.highcharts({
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false
    },
    title: {
        text: 'Assignments by Assignee'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.count} ({point.percentage:.1f}%)</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        type: 'pie',
        name: 'Assignments',
        data: data
    }]
  });
}
check(go);
})();